/*
 * Onur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Onur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Onur. If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <list>
#include <optional>
#include <print>
#include <string>

#include <rapidjson/document.h>

#include "../include/cli.hpp"
#include "../include/konfig.hpp"
#include "../include/parse.hpp"
#include "../include/project.hpp"

using std::ifstream;
using std::istreambuf_iterator;
using std::list;
using std::map;
using std::string;

Parse::Parse () {}

auto
Parse::multi (void) -> list<Konfig>
{
  auto configs_{ repository.all_configs () };
  list<Konfig> result;

  std::print ("configurations: [");
  for (auto config_ : configs_)
    {
      auto single_{ single (contents_of (config_), config_.stem ()) };

      if (single_.has_value ())
        {
          result.push_back (single_.value ());
          std::print (" {} ", config_.stem ().c_str ());
        }
    }
  std::print ("]\n");

  return result;
}

auto
Parse::single (std::string config_content,
               std::string filename) -> std::optional<Konfig>
{
  Konfig konfig;
  map<string, list<Project>> topics;
  konfig.name = { filename };

  auto config_parsed{ parse_file (config_content) };
  if (!config_parsed.IsObject ())
    return std::nullopt;

  if (config_parsed.GetObject ().MemberCount () == 0)
    return std::nullopt;

  for (auto &topic : config_parsed.GetObject ())
    {
      list<Project> projects;
      std::string topic_name = topic.name.GetString ();
      const rapidjson::Value &topic_projects = topic.value;

      if (!topic_projects.IsArray () || topic_projects.Empty ())
        continue;

      for (auto &project : topic_projects.GetArray ())
        {
          if (project.FindMember ("name") == project.MemberEnd ())
            continue;
          if (project.FindMember ("url") == project.MemberEnd ())
            continue;

          auto name = project.FindMember ("name")->value.GetString ();
          auto url = project.FindMember ("url")->value.GetString ();
          auto branch = project.FindMember ("branch") != project.MemberEnd ()
                            ? project.FindMember ("branch")->value.GetString ()
                            : "master"; // check if values are string
          Project projekt{ .name = name, .url = url, .branch = branch };

          projects.push_back (projekt);
        }

      topics[topic_name] = { projects };
    }

  // ignore empty configs
  for (auto topic_ : topics)
    {
      if (topic_.second.empty () || topic_.second.size () == 0)
        return std::nullopt;
    }

  konfig.topics = { topics };

  return konfig;
}

auto
Parse::parse_file (string config_content) -> rapidjson::Document
{
  rapidjson::Document result;
  result.Parse (config_content.c_str ());

  return result;
}

auto
Parse::contents_of (string filepath) -> string
{
  ifstream file_content (filepath);
  return { istreambuf_iterator<char> (file_content),
           istreambuf_iterator<char>{} };
}

auto
Parse::exist (std::string name) -> bool
{
  for (auto config : multi ())
    if (config.name == name)
      return true;

  // std::for_each (multi ().begin (), multi ().end (),
  //                [name, &result] (Konfig config) {
  //                  std::println ("MEH");
  //                  result = { config.name == name };
  //                  std::println ("FOOL");
  //                });

  return false;
}

auto
Parse::save (std::string name, std::string topic,
             ConfigEntries entries) -> void
{
  // if (entries.name || entries.url.has_value () || !entries.branch.empty
  // ())
  //   {
  //     std::println ("Either name or url of project are missing.
  //     Exiting!"); return;
  //   }

  // Project project{ entries };

  // std::map<std::string, std::list<Project>> topics;
  // topics[topic] = { entries };

  // Konfig konfig{ name, topics };
}
