# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

include(FetchContent)

set(CATCH_INSTALL_DOCS  ON CACHE BOOL "" FORCE)
set(CATCH_INSTALL_EXTRAS  ON CACHE BOOL "" FORCE)
set(CATCH_ENABLE_REPRODUCIBLE_BUILD  ON CACHE BOOL "" FORCE)

FetchContent_Declare(
  catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG v3.6.0
)
FetchContent_MakeAvailable(catch2)

include_directories(${PROJECT_SOURCE_DIR}/src/include)
file(GLOB_RECURSE TEST_FILES "test_*.cpp")
file(GLOB_RECURSE SRC_FILES "${PROJECT_SOURCE_DIR}/src/**/*.cpp")
add_executable(onur_tests ${TEST_FILES} ${SRC_FILES})
target_include_directories(onur_tests PRIVATE ${PROJECT_SOURCE_DIR}/src/include)
target_link_libraries(onur_tests PRIVATE Catch2::Catch2WithMain)

enable_testing()

include(CTest)
include(Catch)
# catch_discover_tests(tests)

# target_include_directories(onur_tests PRIVATE ${PROJECT_SOURCE_DIR}/src/include)
# add_test(onur_tests MyTests COMMAND onur_tests)
