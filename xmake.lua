-- Set the project name and version
set_project("onur")
set_version("0.1.0")

-- Set the C++ standard to C++23
set_languages("cxx23")

-- Add external dependencies (must be in root scope)
add_requires("cli11", "rapidjson", "catch2")

-- Define the main target
target("main")
    set_kind("binary") -- Build an executable
    add_files("src/main.cpp", "src/**.cpp") -- Source files, excluding tests
    add_includedirs("src/include") -- Include headers
    add_packages("cli11", "rapidjson") -- Link with cli11 and rapidjson

-- Define the tests target
target("tests")
    set_kind("binary") -- Build an executable
    add_files("tests/*.cpp") -- Test files
    add_files("src/**.cpp") -- Include all .cpp files from src and subdirectories
    add_includedirs("src/include") -- Include headers
    add_packages("catch2") -- Link with dependencies for tests
    add_defines("CATCH_CONFIG_MAIN") -- Optional: Define Catch2 main entry point

-- Default target to build and test
task("run_tests")
    set_menu {
        usage = "xmake run_tests",
        description = "Run all tests",
    }
    on_run(function ()
        os.exec("xmake run tests")
    end)
